* docker engine version 18.02+
* pull public.ecr.aws/cldevops/go:1.22
* clone bitbucket.org/clearlinkit/docker-training

# Preface: 

container runtime ecosystem goes well beyond Docker Engine. "docker" is actually a
collection of separate tools (containerd, runC) that let you build, fetch, and run images. each of
those steps have many other options in the ecosystem now.

# Why Docker?

* Abstract applications from environment
* Consistent deployments
* Applications work similarly in dev vs prod
* Better utilization of resources

# What are we doing?

## Running

* docker run busybox
* docker run busybox echo "hello!"
* docker run busybox ls -la
* docker ps [-a]
* docker run busybox bash
* docker run busybox sh
* docker run -i busybox sh
* docker run -it busybox sh

* docker run busybox sleep 100

* docker ps
* docker stop [kill]

* docker container ls
* docker image ls

## Image format / tag:

* docker run busybox:latest echo "hello"
* docker run busybox:1.31 echo "hello"

## Basic Networking

* docker run nginx:1.16-alpine
* docker run -p 8090:80 nginx:1.16-alpine

## Building an Image

* cd demo-basic
* Explain Dockerfile
    * entrypoint, command
* docker build -t cl-nginx .
* docker run -p 8090:80 cl-nginx

## Exec-ing into a container

* docker ps
* edit index.html
* docker exec -it <hash> sh
* vi /usr/share/nginx/html/index.html
* docker build -t cl-nginx .
* docker kill <hash>
* docker run -p 8090:80 cl-nginx

## Mounting a volume

* docker run -p 8090:80 -v `pwd`/static/volume.html:/usr/share/nginx/html/index.html nginx:1.16-alpine
* edit index.html
* docker exec -it <hash> sh
* vi /usr/share/nginx/html/index.html
* volumes vs build time

## Environment Variables

* docker run -p 8090:80 cl-nginx
* docker run -p 8090:80 -e MESSAGE="Some Message" cl-nginx

## docker-compose

### basics

* wrapper

* docker-compose up [-d]
* docker-compose down
* docker logs
* docker-compose run -e MESSAGE="Message from Run" training-nginx
* docker-compose run --service-ports -e MESSAGE="Message from Run" training-nginx
* docker-compose run -e MESSAGE="Another message from Run" training-nginx cat /usr/share/nginx/html/index.html

### networking / configuration

* cd demo-multi
* docker-compose up
* localhost:8090/ping
* localhost:8090/proxy/node-2

* docker-compose down
* docker-compose -f ab/docker-compose.yml up -d
* docker-compose -f c/docker-compose.yml up -d
* docker logs -f ab_node-1_1
* localhost:8090/proxy/node-2
* localhost:8090/proxy/node-3
* docker-compose -f ab/docker-compose.yml down
* docker-compose -f c/docker-compose.yml down
* docker network create docker-training
* docker-compose -f ab/docker-compose.yml up -d
* docker-compose -f c/docker-compose.yml up -d
* localhost:8090/proxy/node-2
* localhost:8090/proxy/node-3

## Real-world setup

* check for anything binding port 80
* cd shared
* docker-compose up -d
* cd ../laravel
* docker build --target base -t demo-app:base .
* docker-compose run --rm demo-php composer install
* docker-compose run --rm demo-php php artisan migrate
* docker-compose up

* what is a docker image?
    * basically a template for creating a docker container
    * many containers can be run off of one image

* docker run
    * docker container run training-app go run main.go 
    * docker run training-app go run main.go 
    * environment variables
    * what is a docker container?
        * Running linux system. Isolated filesystem, process list, CPU / Memory resources
        * NOT a VM: isolation is at a much higher level, in the kernel
        * "relatively" isolated from other running containers: how isolated is configurable (network,
        storage)
        * Very low overhead
            * at least when run natively on Linux
            
* what is a docker volume?
    * docker run public.ecr.aws/cldevops/go:1.22 ls -la /app
    * docker run -v $(pwd):/app public.ecr.aws/cldevops/go:1.22 ls -la /app
    * bind mount vs named volume
    * consistent vs cached vs delegated

* docker-compose
    * docker-compose run --rm training-app go run main.go hello
    * go through docker-compose.yml
    * docker-compose run --rm training-app ./training
    * docker-compose up [-d]
    * local: docker swarm / ECS / k8s
    
* multi-stage builds
    
* general use
    * docker ps / docker container ls
        * docker ps -a | grep <what you're looking for> # grab the logs for a dead thing
    * docker kill <id>
    * docker prune / docker image prune / docker volume prune / docker container prune / docker rmi
    * shelling in: docker exec / docker run
    
* aliases

docker build --target base -t training-app:environment .

docker-compose run --rm training-app go run main.go hello

docker-compose run -e HELLO_MESSAGE="something else" training-app go run main.go server
docker-compose run -e HELLO_MESSAGE="something else" --service-ports training-app go run main.go server
