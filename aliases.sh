alias d='docker'
alias dc='docker-compose'
alias dcr='dc run --rm'
alias dcrp='dcr --service-ports'
alias dce='dc exec'
alias dps="docker ps"
alias dl="docker logs"
alias dk="docker kill"
alias dka='docker kill $(docker ps -q)'

function dsh {
    docker run -it $1 sh
}

function desh {
    docker exec -it $1 sh
}
