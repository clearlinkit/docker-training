#!/bin/sh
set -e
echo "I'm an entrypoint! Arguments provided to me: '$@'"

# envsubst replaces environment variables with their values
# need to go to a temporary file, since reading + redirecting to the same file = blank
cat "${INDEX_FILE:-/usr/share/nginx/html/index.html}" | envsubst > /tmp.html
cp -f /tmp.html /usr/share/nginx/html/index.html

# exec _replaces_ the current program in the current process
exec "$@"