#!/usr/bin/env sh

if [ "$IGNORE_LOGS" != "1" ]; then
    /logs.sh
fi

# emulate crontab in docker
while [ true ]
do
    php /app/artisan schedule:run --verbose --no-interaction &
    sleep 60
done
