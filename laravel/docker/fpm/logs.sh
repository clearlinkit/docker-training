#!/usr/bin/env sh

touch /app/storage/logs/lumen.log && tail -f /app/storage/logs/lumen.log &
touch /app/storage/logs/invoca.log && tail -f /app/storage/logs/invoca.log &
touch /app/storage/logs/botcache.log && tail -f /app/storage/logs/botcache.log &
touch /app/storage/logs/audit.log && tail -f /app/storage/logs/audit.log &

touch /var/log/php-fpm.log && tail -f /var/log/php-fpm.log &
touch /var/log/php_error.log && tail -f /var/log/php_error.log &
