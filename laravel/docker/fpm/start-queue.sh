#!/usr/bin/env bash

/logs.sh

cd /app

processes=${QUEUE_PROCESSES:-1}

for i in $(seq 1 $processes); do
    echo "running queue worker"
    php artisan queue:listen --queue=$QUEUES --tries=3 --timeout=${QUEUE_TIMEOUT_SECONDS:-60} &
done

wait -n
echo "at least one queue worker exited, exiting all"
kill 0
