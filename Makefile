test:
	@docker pull busybox
	@docker pull public.ecr.aws/cldevops/go:1.22
	@docker pull alpine:3.11
	@docker pull nginx:1.16-alpine
	@docker run busybox echo "docker run seems to work!"



PIPELINE_IMAGE ?= "public.ecr.aws/cldevops/pipeline-tools"

PIPELINE_BASE = docker run --rm -v $$(pwd):/app -w /app
PIPELINE = ${PIPELINE_BASE} -e AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY -e AWS_SESSION_TOKEN -e "AWS_REGION=$(AWS_REGION)" \
  -e "ENVIRONMENT_NAME=$(ENVIRONMENT_NAME)" \
  $(PIPELINE_IMAGE)

CFN_ARGS ?= --auto


test-cli:
	${PIPELINE} aws ec2 describe-regions


test-cfn:
	${PIPELINE_BASE} $(PIPELINE_IMAGE) cfn-lint /app/ops/cloudformation/*.yml

env-validate:
	@test $(ENVIRONMENT_NAME) || (echo "no ENVIRONMENT_NAME"; exit 1)
	@test $(AWS_REGION) || (echo "no AWS_REGION"; exit 1)
	@echo " AWS_REGION=$(AWS_REGION)\n\
	 ENVIRONMENT_NAME=$(ENVIRONMENT_NAME)"
	 @read -p "Continue? (y/n): " continue; \
	if [ "$$continue" != "y" ]; then exit 1; fi; \


cfn-network: env-validate test-cfn
	${PIPELINE} cfn-from-json \
        --input ops/$(ENVIRONMENT_NAME)/network-stack.json \
        --template ops/cloudformation/network-stack.yml \
        -v \
	    $(CFN_ARGS)

cfn-infrastructure: env-validate test-cfn
	${PIPELINE} cfn-from-json \
        --input ops/$(ENVIRONMENT_NAME)/infrastructure-stack.json \
        --template ops/cloudformation/infrastructure-stack.yml \
        -v \
	    $(CFN_ARGS)

cfn-app: env-validate test-cfn
	${PIPELINE} cfn-from-json \
        --input ops/$(ENVIRONMENT_NAME)/app-stack.json \
        --template ops/cloudformation/app-stack.yml \
        -v \
	    $(CFN_ARGS)
