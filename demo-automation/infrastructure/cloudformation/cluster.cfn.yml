AWSTemplateFormatVersion: 2010-09-09
Description: ECS Cluster

Parameters:
  ClusterName:
    Type: String
    Description: Cluster name

  ParentVpcStack:
    Description: VPC the ECS cluster should be deployed to
    Type: String

  ClusterSecurityGroups:
    Description: List of security groups to attach to the cluster instances
    Type: String
    Default: ""

  ClusterInstanceType:
    Type: String
    Default: t3.medium

  ClusterMinSize:
    Type: Number
    Default: 1

  ClusterMaxSize:
    Type: Number
    Default: 5

  ClusterDesiredSize:
    Type: Number
    Default: 2

  AsgUpdateMaxBatchSize:
    Type: Number
    Default: 1

  AsgUpdateMinInstances:
    Type: Number
    Default: 1

  ECSAMI:
    Description: ECS-Optimized AMI ID
    Type: AWS::SSM::Parameter::Value<AWS::EC2::Image::Id>
    Default: /aws/service/ecs/optimized-ami/amazon-linux-2/recommended/image_id

Metadata:
  cfn-lint:
    config:
      ignore_checks:
        # ignore VpcId type suggestion and warnings
        - E3008
        - W

Conditions:
  HasExtraSGs:
    Fn::Not:
      - !Equals [!Ref ClusterSecurityGroups, ""]

Resources:

  ECSCluster:
    Type: AWS::ECS::Cluster
    Properties:
      ClusterName: !Ref ClusterName

  ECSAutoScalingGroup:
    DependsOn: ECSCluster
    Type: AWS::AutoScaling::AutoScalingGroup
    Properties:
      VPCZoneIdentifier: 
        - Fn::ImportValue: !Sub "${ParentVpcStack}-SubnetAPrivate"
        - Fn::ImportValue: !Sub "${ParentVpcStack}-SubnetBPrivate"
      LaunchConfigurationName: !Ref ECSLaunchConfiguration
      MinSize: !Ref ClusterMinSize
      MaxSize: !Ref ClusterMaxSize
      DesiredCapacity: !Ref ClusterDesiredSize
      MetricsCollection:
        - Granularity: 1Minute
      Tags:
        - Key: Name
          Value: !Sub "${AWS::StackName}-node"
          PropagateAtLaunch: true
    CreationPolicy:
      ResourceSignal:
        Timeout: PT15M
    UpdatePolicy:
      AutoScalingRollingUpdate:
        MinInstancesInService: !Ref AsgUpdateMinInstances
        MaxBatchSize: !Ref AsgUpdateMaxBatchSize
        PauseTime: PT15M
        SuspendProcesses:
          - HealthCheck
          - ReplaceUnhealthy
          - AZRebalance
          - AlarmNotification
          - ScheduledActions
        WaitOnResourceSignals: true

  ScaleUpPolicy:
    Type: AWS::AutoScaling::ScalingPolicy
    Properties:
      AdjustmentType: ChangeInCapacity
      AutoScalingGroupName: !Ref ECSAutoScalingGroup
      Cooldown: '120'
      ScalingAdjustment: 1

#  CPUAlarmHigh:
#    Type: AWS::CloudWatch::Alarm
#    Properties:
#      EvaluationPeriods: 3
#      Statistic: Average
#      Threshold: 70
#      AlarmDescription: Alarm if CPU too high or metric disappears indicating instance
#        is down
#      Period: 60
#      AlarmActions:
#        - !Ref ScaleUpPolicy
#      Namespace: AWS/ECS
#      Dimensions:
#        - Name: ClusterName
#          Value: !Ref ECSCluster
#      ComparisonOperator: GreaterThanThreshold
#      MetricName: CPUUtilization
#
#  CPUReservationTooHigh:
#    Type: AWS::CloudWatch::Alarm
#    Properties:
#      EvaluationPeriods: 3
#      Statistic: Average
#      Threshold: 70
#      AlarmDescription: Alarm if reserved memory too high or metric disappears indicating instance
#        is down
#      Period: 60
#      AlarmActions:
#        - !Ref ScaleUpPolicy
#      Namespace: AWS/ECS
#      Dimensions:
#        - Name: ClusterName
#          Value: !Ref ECSCluster
#      ComparisonOperator: GreaterThanThreshold
#      MetricName: CPUReservation
#
#  MemoryUtilizationTooHigh:
#    Type: AWS::CloudWatch::Alarm
#    Properties:
#      EvaluationPeriods: 3
#      Statistic: Average
#      Threshold: 80
#      AlarmDescription: Alarm if utilized memory too high or metric disappears indicating instance
#        is down
#      Period: 60
#      AlarmActions:
#        - !Ref ScaleUpPolicy
#      Namespace: AWS/ECS
#      Dimensions:
#        - Name: ClusterName
#          Value: !Ref ECSCluster
#      ComparisonOperator: GreaterThanThreshold
#      MetricName: MemoryUtilization
#
#  MemoryReservationTooHigh:
#    Type: AWS::CloudWatch::Alarm
#    Properties:
#      EvaluationPeriods: 3
#      Statistic: Average
#      Threshold: 70
#      AlarmDescription: Alarm if reserved memory too high or metric disappears indicating instance
#        is down
#      Period: 60
#      AlarmActions:
#        - !Ref ScaleUpPolicy
#      Namespace: AWS/ECS
#      Dimensions:
#        - Name: ClusterName
#          Value: !Ref ECSCluster
#      ComparisonOperator: GreaterThanThreshold
#      MetricName: MemoryReservation

  LbSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      VpcId:
        Fn::ImportValue: !Sub ${ParentVpcStack}-VPC
      GroupDescription: !Sub ${ClusterName} ECS cluster nodes
      Tags:
        - Key: Name
          Value: !Sub ${ClusterName}-LoadBalancer-SG
        - Key: Description
          Value: Attach this SG to a load balancer to allow traffic into the cluster

  ClusterSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      VpcId:
        Fn::ImportValue: !Sub ${ParentVpcStack}-VPC
      GroupDescription: !Sub ${ClusterName} ECS cluster nodes
      SecurityGroupEgress:
        - CidrIp: 0.0.0.0/0
          IpProtocol: "-1"
      SecurityGroupIngress:
        - SourceSecurityGroupId: !Ref LbSecurityGroup
          IpProtocol: "-1"
      Tags:
        - Key: Name
          Value: !Sub ${ClusterName}-Node-SG

  ECSLaunchConfiguration:
    Type: AWS::AutoScaling::LaunchConfiguration
    Properties:
      ImageId: !Ref ECSAMI
      InstanceType: !Ref ClusterInstanceType
      SecurityGroups: !If
        - HasExtraSGs
        - !Split
          - ","
          - !Sub "${ClusterSecurityGroups},${ClusterSecurityGroup}"
        -
          - !Ref ClusterSecurityGroup
      IamInstanceProfile: !Ref ECSInstanceProfile
      UserData:
        "Fn::Base64": !Sub |
          #!/bin/bash
          yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm
          yum install -y https://s3.amazonaws.com/amazoncloudwatch-agent/amazon_linux/amd64/latest/amazon-cloudwatch-agent.rpm

          echo ECS_CLUSTER=${ECSCluster} >> /etc/ecs/ecs.config

          yum install -y aws-cfn-bootstrap hibagent awslogs

          /opt/aws/bin/cfn-init -v --region ${AWS::Region} --stack ${AWS::StackName} --resource ECSLaunchConfiguration
          /opt/aws/bin/cfn-signal -e $? --region ${AWS::Region} --stack ${AWS::StackName} --resource ECSAutoScalingGroup
    Metadata:
      AWS::CloudFormation::Init:
        config:
          commands:
            01_enable_cfnhup:
              command: systemctl enable cfn-hup.service
            02_start_cfnhup:
              command: systemctl start cfn-hup.service
            03_enable_cloudwatch_agent:
              command: !Sub /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -c ssm:${ECSCloudWatchParameter} -s
          files:
            /etc/cfn/cfn-hup.conf:
              mode: 000400
              owner: root
              group: root
              content: !Sub |
                [main]
                stack=${AWS::StackId}
                region=${AWS::Region}

            /etc/cfn/hooks.d/cfn-auto-reloader.conf:
              content: !Sub |
                [cfn-auto-reloader-hook]
                triggers=post.update
                path=Resources.ECSLaunchConfiguration.Metadata.AWS::CloudFormation::Init
                action=/opt/aws/bin/cfn-init -v --region ${AWS::Region} --stack ${AWS::StackName} --resource ECSLaunchConfiguration

            /lib/systemd/system/cfn-hup.service:
              content: !Sub |
                [Unit]
                Description=cfn-hup daemon
                [Service]
                Type=simple
                ExecStart=/opt/aws/bin/cfn-hup
                Restart=always
                [Install]
                WantedBy=multi-user.target

  # This IAM Role is attached to all of the ECS hosts. It is based on the default role
  # published here:
  # http://docs.aws.amazon.com/AmazonECS/latest/developerguide/instance_IAM_role.html
  #
  # You can add other IAM policy statements here to allow access from your ECS hosts
  # to other AWS services. Please note that this role will be used by ALL containers
  # running on the ECS host.
  ECSRole:
    Type: AWS::IAM::Role
    Properties:
      Path: /
      RoleName: !Sub ${ClusterName}-ECSRole-${AWS::Region}
      AssumeRolePolicyDocument: |
        {
            "Statement": [{
                "Action": "sts:AssumeRole",
                "Effect": "Allow",
                "Principal": {
                    "Service": "ec2.amazonaws.com"
                }
            }]
        }
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore
        - arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy
      Policies:
        - PolicyName: ssm-s3
          PolicyDocument:
            Statement:
              - Effect: Allow
                Resource:
                  - !Sub "arn:aws:s3:::aws-ssm-${AWS::Region}/*"
                  - !Sub "arn:aws:s3:::aws-windows-downloads-${AWS::Region}/*"
                  - !Sub "arn:aws:s3:::amazon-ssm-${AWS::Region}/*"
                  - !Sub "arn:aws:s3:::amazon-ssm-packages-${AWS::Region}/*"
                  - !Sub "arn:aws:s3:::${AWS::Region}-birdwatcher-prod/*"
                  - !Sub "arn:aws:s3:::patch-baseline-snapshot-${AWS::Region}/*"
                Action: "s3:GetObject"
        - PolicyName: ecs-service
          PolicyDocument:
            Statement:
              - Effect: Allow
                Resource: "*"
                Action:
                    - ecr:BatchCheckLayerAvailability
                    - ecr:BatchGetImage
                    - ecr:GetDownloadUrlForLayer
                    - ecr:GetAuthorizationToken
              - Effect: "Allow"
                Resource: "*"
                Action:
                  - ecs:DiscoverPollEndpoint
                  - ecs:Poll
                  - ecs:StartTelemetrySession
                  - logs:CreateLogStream
                  - logs:PutLogEvents
              - Effect: "Allow"
                Resource: !GetAtt ECSCluster.Arn
                Action:
                  - ecs:DeregisterContainerInstance
                  - ecs:RegisterContainerInstance
                  - ecs:Submit*

  ECSInstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Path: /
      Roles:
        - !Ref ECSRole

  ECSServiceAutoScalingRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          Action:
            - "sts:AssumeRole"
          Effect: Allow
          Principal:
            Service:
              - application-autoscaling.amazonaws.com
      Path: /
      Policies:
        - PolicyName: ecs-service-autoscaling
          PolicyDocument:
            Statement:
              Effect: Allow
              Action:
                - application-autoscaling:*
                - cloudwatch:DescribeAlarms
                - cloudwatch:PutMetricAlarm
                - ecs:DescribeServices
                - ecs:UpdateService
              Resource: "*"

  ECSCloudWatchParameter:
    Type: AWS::SSM::Parameter
    Properties:
      Description: ECS
      Name: !Sub "AmazonCloudWatch-${ECSCluster}-ECS"
      Type: String
      Value: !Sub |
        {
          "logs": {
            "force_flush_interval": 5,
            "logs_collected": {
              "files": {
                "collect_list": [
                  {
                    "file_path": "/var/log/messages",
                    "log_group_name": "${ECSCluster}-/var/log/messages",
                    "log_stream_name": "{instance_id}",
                    "timestamp_format": "%b %d %H:%M:%S"
                  },
                  {
                    "file_path": "/var/log/dmesg",
                    "log_group_name": "${ECSCluster}-/var/log/dmesg",
                    "log_stream_name": "{instance_id}"
                  },
                  {
                    "file_path": "/var/log/docker",
                    "log_group_name": "${ECSCluster}-/var/log/docker",
                    "log_stream_name": "{instance_id}",
                    "timestamp_format": "%Y-%m-%dT%H:%M:%S.%f"
                  },
                  {
                    "file_path": "/var/log/ecs/ecs-init.log",
                    "log_group_name": "${ECSCluster}-/var/log/ecs/ecs-init.log",
                    "log_stream_name": "{instance_id}",
                    "timestamp_format": "%Y-%m-%dT%H:%M:%SZ"
                  },
                  {
                    "file_path": "/var/log/ecs/ecs-agent.log.*",
                    "log_group_name": "${ECSCluster}-/var/log/ecs/ecs-agent.log",
                    "log_stream_name": "{instance_id}",
                    "timestamp_format": "%Y-%m-%dT%H:%M:%SZ"
                  },
                  {
                    "file_path": "/var/log/ecs/audit.log",
                    "log_group_name": "${ECSCluster}-/var/log/ecs/audit.log",
                    "log_stream_name": "{instance_id}",
                    "timestamp_format": "%Y-%m-%dT%H:%M:%SZ"
                  }
                ]
              }
            }
          }
        }


Outputs:

  ClusterName:
    Value: !Ref ClusterName
    Export:
      Name: !Sub "${AWS::StackName}-ClusterName"

  InboundSecurityGroup:
    Value: !Ref LbSecurityGroup
    Export:
      Name: !Sub "${AWS::StackName}-InboundSecurityGroup"
    Description: Attach this SG to a load balancer to allow traffic into the cluster

  ClusterSecurityGroup:
    Description: "Security group assigned to the cluster nodes. Use to identify traffic coming from the cluster"
    Value: !Ref ClusterSecurityGroup
    Export:
      Name: !Sub '${AWS::StackName}-ClusterSecurityGroup'

  VpcStack:
    Value: !Ref ParentVpcStack
    Export:
      Name: !Sub "${AWS::StackName}-VpcStack"
    Description: Name of the VPC stack this cluster is deployed on


