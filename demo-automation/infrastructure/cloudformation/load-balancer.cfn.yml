AWSTemplateFormatVersion: 2010-09-09
Description: Shared ECS Load Balancer

Parameters:
  ClusterStackName1:
    Type: String
    Description: Cluster name 1 (for inbound security groups)

  ClusterStackName2:
    Type: String
    Description: Cluster name 2 (for inbound security groups)
    Default: ":none:"

  ParentVpcStack:
    Description: VPC the ECS cluster should be deployed to
    Type: String

  LoadBalancerSecurityGroups:
    Description: Extra SGs to attach to the load balancer
    Type: String
    Default: ":none:"

  LoadBalancerCertArn1:
    Description: Certificate ARN 1 for SSL
    Type: String
    Default: ":none:"

  LoadBalancerCertArn2:
    Description: Certificate ARN 2 for SSL
    Type: String
    Default: ":none:"

  LoadBalancerCertArn3:
    Description: Certificate ARN 3 for SSL
    Type: String
    Default: ":none:"

  # If you need more, just add 'em.
  LoadBalancerCertArn4:
    Description: Certificate ARN 4 for SSL
    Type: String
    Default: ":none:"

Metadata:
  cfn-lint:
    config:
      ignore_checks:
        # ignore VpcId type suggestion and warnings
        - E3008
        - W6001

Conditions:
  HasExtraSgs:
    Fn::Not:
      - !Equals [!Ref LoadBalancerSecurityGroups, ":none:"]
  HasCert2:
    Fn::Not:
      - !Equals [!Ref LoadBalancerCertArn2, ":none:"]
  HasCert3:
    Fn::Not:
      - !Equals [!Ref LoadBalancerCertArn3, ":none:"]
  HasCert4:
    Fn::Not:
      - !Equals [!Ref LoadBalancerCertArn4, ":none:"]
  HasCluster2:
    Fn::Not:
      - !Equals [!Ref ClusterStackName2, ":none:"]

Resources:

  WebBalancerSG:
    Type: AWS::EC2::SecurityGroup
    Properties:
      VpcId:
        Fn::ImportValue: !Sub "${ParentVpcStack}-VPC"
      GroupDescription: !Sub ${AWS::StackName} LB SG
      SecurityGroupIngress:
        - CidrIp: 0.0.0.0/0
          FromPort: 80
          ToPort: 80
          IpProtocol: "-1"
        - CidrIp: 0.0.0.0/0
          FromPort: 443
          ToPort: 443
          IpProtocol: "-1"
      Tags:
        - Key: Name
          Value: !Sub "${AWS::StackName}-LB"

  LoadBalancer:
    Type: AWS::ElasticLoadBalancingV2::LoadBalancer
    Properties:
      Name: !Sub ${AWS::StackName}-LB
      SecurityGroups: !If
        - HasExtraSgs
        - !Split
          - ","
          - !Sub
            - "${LoadBalancerSecurityGroups},${WebBalancerSG},${IbSecurityGroup}"
            - IbSecurityGroup: !Join
              - "," 
              - - Fn::ImportValue: !Sub "${ClusterStackName1}-InboundSecurityGroup"
                - !If 
                  - HasCluster2
                  - Fn::ImportValue: !Sub "${ClusterStackName2}-InboundSecurityGroup"
                  - !Ref AWS::NoValue
        - - !Ref WebBalancerSG
          - Fn::ImportValue: !Sub "${ClusterStackName1}-InboundSecurityGroup"
          - !If
            - HasCluster2
            - Fn::ImportValue: !Sub "${ClusterStackName2}-InboundSecurityGroup"
            - !Ref AWS::NoValue
      Subnets:
        - Fn::ImportValue: !Sub "${ParentVpcStack}-DMZSubnet1"
        - Fn::ImportValue: !Sub "${ParentVpcStack}-DMZSubnet2"

  SslListener:
    Type: AWS::ElasticLoadBalancingV2::Listener
    Properties:
      Certificates:
        - CertificateArn: !Ref LoadBalancerCertArn1
      DefaultActions:
        - Type: fixed-response
          FixedResponseConfig:
            StatusCode: "404"
            MessageBody: '{"error": "Hit default listener"}'
            ContentType: application/json
      LoadBalancerArn: !Ref LoadBalancer
      Port: 443
      Protocol: HTTPS
      SslPolicy: ELBSecurityPolicy-TLS-1-2-2017-01

  SslListenerCert2:
    Type: AWS::ElasticLoadBalancingV2::ListenerCertificate
    Condition: HasCert2
    Properties:
      ListenerArn: !Ref SslListener
      Certificates:
        - CertificateArn: !Ref LoadBalancerCertArn2

  SslListenerCert3:
    Type: AWS::ElasticLoadBalancingV2::ListenerCertificate
    Condition: HasCert3
    Properties:
      ListenerArn: !Ref SslListener
      Certificates:
        - CertificateArn: !Ref LoadBalancerCertArn3

  SslListenerCert4:
    Type: AWS::ElasticLoadBalancingV2::ListenerCertificate
    Condition: HasCert4
    Properties:
      ListenerArn: !Ref SslListener
      Certificates:
        - CertificateArn: !Ref LoadBalancerCertArn4

  HttpListener:
    Type: AWS::ElasticLoadBalancingV2::Listener
    Properties:
      DefaultActions:
        - Type: redirect
          RedirectConfig:
            Host: "#{host}"
            Path: "/#{path}"
            Port: "443"
            Protocol: HTTPS
            Query: "#{query}"
            StatusCode: HTTP_301
      LoadBalancerArn: !Ref LoadBalancer
      Port: 80
      Protocol: HTTP

Outputs:

  VpcStackName:
    Value: !Ref ParentVpcStack
    Export:
      Name: !Sub "${AWS::StackName}-VpcStackName"
    Description: Name of the VPC stack this cluster is deployed on

  ClusterStack:
    Value: !Ref ClusterStackName1
    Export:
      Name: !Sub "${AWS::StackName}-ClusterStackName"
    Description: Name of the VPC stack this cluster is deployed on

  ClusterStack2:
    Value: !Ref ClusterStackName2
    Export:
      Name: !Sub "${AWS::StackName}-ClusterStackName2"
    Description: Name of the VPC stack this cluster is deployed on

  LoadBalancerArn:
    Value: !Ref LoadBalancer
    Export:
      Name: !Sub "${AWS::StackName}-LoadBalancerArn"
    Description: Load balancer ARN

  LoadBalancerDNSName:
    Value: !GetAtt LoadBalancer.DNSName
    Export:
      Name: !Sub "${AWS::StackName}-LoadBalancerDNSName"
    Description: Load balancer A Record

  SslListenerArn:
    Value: !Ref SslListener
    Export:
      Name: !Sub "${AWS::StackName}-SslListener"
    Description: SSL Listener ARN

  LoadBalancerSecurityGroup:
    Value: !Ref WebBalancerSG
    Export:
      Name: !Sub "${AWS::StackName}-LoadBalancerSecurityGroup"
    Description: Load balancer security group (used to whitelist communication from the LB)


