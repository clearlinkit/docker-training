## End-To-End Automation

### Basics

* End-to-end
    * Pipeline triggered on Bitbucket (by merge, manual triggering, whatever)
    * Bitbucket builds an app image, and pushes it up to the registry with a unique tag (commit hash)
        * Why push from Bitbucket? BB handles CI, and devs are more used to interacting with / have more access
          to view things on BB. Easier to have devs responsible for their own problems if it all happens there.
    * Bitbucket places the code in an S3 bucket, which triggers a CodePipeline for the environment
    * CodePipeline creates a changeset for the Cloudformation (cfn)
        * Since the commit hash changes with every deploy, it is guaranteed to need to update the image
          on the ECS task definitions
    * After manual approval, CodePipeline calls out to CodeBuild to run any tasks that need to happen before restarting 
      the ECS services (e.g., import new parameter store variables, run migrations)
    * CodePipeline then executes the cfn change set
        * This triggers ECS to execute a deployment, spinning up new containers and killing old ones

### Steps

* Step zero: make sure you're not on the VPN

* Open a separate terminal and start the build for this, just in case you don't have the images
  you need locally:
  
    $ make final

* Make sure you can talk to AWS. https://cdn.clear.link/mfa.sh

    $ aws sts get-caller-identity
    
* Log in to the AWS console in your browser, cause we're gonna need it.

* Open up Makefile here, and set APP_NAME to something short, unlikely to collide with anyone
  else, and valid for a URL:
  
    $ export APP_NAME = wes-curtis-echo
  
* Set up your own personal pipeline:

    $ $(make configure-training)
    $ make cfn-pipeline
    # then click / cmd + click / ctrl + click / whatever the link that comes up
    
* Go through presumed infrastructure:

  * VPC
  * ECS Cluster
  * Load Balancer
  
* Go through Pipeline infrastructure:

  * IAM Role
  * ECR
  * CodePipeline
    * S3 Buckets
  * CodeBuild
    * Task Definition
    
* Push your app image up to your registry:

    $ $(make ecr-login)
    $ REGISTRY=$(make registry) make push
    
* Trigger a deployment through the S3 bucket:

    $ make trigger-deploy
    # then wait an indeterminate amount of time
    
* Set up bitbucket pipelines

    $ make create-access-key
    # this will make a access key + secret access key for the bitbucket pipelines user
    
    * Go to https://bitbucket.org/clearlinkit/docker-training
    * Fork the repo (click the big '+' on the left side of the screen, click 'Fork')
    * Fill in whatever you'd like for project
    * Click the `Pipelines` link on the left side
        * This just seems to enable the pipelines in the settings, weird
    * Click the `Repository Settings` link on the left side
    * Click `Settings` under `Pipelines`
    * Enable the pipelines
    * Click `Deployments`
    * Click `Add environment`, make one named `training`
    * Add these variables:
        * AWS_REGION=us-east-1
        * ENVIRONMENT_NAME=training
        * AWS_ACCESS_KEY_ID from `make create-access-key` above
        * AWS_SECRET_ACCESS_KEY from `make create-access-key` above
        * REGISTRY from `make registry`
        * CODEPIPELINE_BUCKET from `make get-codepipeline-bucket`
        
    * Click `Pipelines` on the left side
    * Click `Run Pipeline`
    
  
    

