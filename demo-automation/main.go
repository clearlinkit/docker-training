package main

import (
	"github.com/gin-gonic/gin"
	"log"
	"os"
	"strings"
)

func main()  {
	r := gin.New()

	r.GET("/health", func(ctx *gin.Context) {
		ctx.JSON(200, gin.H{"status": "OK"})
	})

	r.GET("/", func(ctx *gin.Context) {
		var safe []string
		for _, e := range os.Environ() {
			if strings.Contains(e, "METADATA") {
				continue
			}
			safe = append(safe, e)
		}
		ctx.JSON(200, gin.H{
			"env": safe,
		})
	})

	if err := r.Run(":8080"); err != nil {
		log.Fatalf("error running gin router: %s", err.Error())
	}
}
