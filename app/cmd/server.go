package cmd

import (
	"bitbucket.org/clearlinkit/docker-training/app"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/spf13/cobra"
	"net/http"
	"os"
)

var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "Runs the HTTP server for the docker training demo-basic project",
	Run: func(cmd *cobra.Command, args []string) {
		router := gin.New()
		router.Use(gin.Recovery())

		msg := os.Getenv("HELLO_MESSAGE")
		router.GET("/", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{"message": msg})
		})

		router.GET("/:key", func(c *gin.Context) {
			val, err := app.GetKey(c.Param("key"))
			if err != nil {
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
				return
			}
			c.JSON(200, gin.H{"value": val})
		})

		router.POST("/:key", func(c *gin.Context) {
			data, err := c.GetRawData()
			if err != nil {
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
				return
			}

			key := c.Param("key")
			val := string(data)
			err = app.SetKey(key, val)
			if err != nil {
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
				return
			}
			c.JSON(http.StatusAccepted, gin.H{"message": fmt.Sprintf("set %s to %s", key, val)})
		})

		if err := router.Run(); err != nil {
			panic(err)
		}
	},
}
