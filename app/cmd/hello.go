package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
)

var helloCmd = &cobra.Command{
	Use:   "hello",
	Short: "Echoes a message",
	Run: func(cmd *cobra.Command, args []string) {
		msg := os.Getenv("HELLO_MESSAGE")
		if msg == "" {
			msg = "hello!"
		}

		fmt.Printf("message: %s\n", msg)
	},
}
