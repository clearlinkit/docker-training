package cmd

import (
	"bitbucket.org/clearlinkit/docker-training/app"
	"github.com/spf13/cobra"
)

var (
	rootCmd = &cobra.Command{
		Use:   "version",
		Short: "Root command for the Clearlink docker training demo-basic project",
	}
)

// Execute executes the root command.
func Execute() error {
	return rootCmd.Execute()
}

func init() {
	cobra.OnInitialize(app.InitApp)

	rootCmd.AddCommand(helloCmd)
	rootCmd.AddCommand(serverCmd)
}
