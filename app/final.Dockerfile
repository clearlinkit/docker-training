FROM public.ecr.aws/cldevops/go:1.22 as environment

WORKDIR /app

RUN apk add --update git ca-certificates
ENV GO111MODULE=on
ENV CGO_ENABLED=0
ENV GOFLAGS="-mod=vendor"
ENV GOPROXY=https://proxy.golang.org
ENV PORT=8080
ENTRYPOINT ["/entrypoint.sh"]

FROM environment as builder

COPY go.mod go.sum /app/
RUN go mod download
COPY . ./
RUN go mod vendor

FROM builder as built
RUN go build -o training

FROM alpine:3.10 as source
WORKDIR /app
RUN apk update && apk add ca-certificates
COPY --from=built /app/training /app/
COPY --from=public.ecr.aws/cldevops/pipeline-tools /usr/local/bin/chamber /usr/local/bin/chamber

COPY entrypoint.sh /entrypoint.sh

EXPOSE 8080
ENTRYPOINT ["/entrypoint.sh"]
CMD ["/app/training", "server"]
