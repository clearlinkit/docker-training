#!/usr/bin/env sh
set -e

echo "I'm an entrypoint! Arguments provided to me: '$@'"
#go mod vendor

exec "$@"