package app

import (
	"errors"
	"github.com/go-redis/redis"
	"github.com/spf13/viper"
	"log"
	"os"
	"strings"
	"time"
)

var (
	redisClient *redis.Client
)

func InitApp() {
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_", ".", "_"))
	viper.AutomaticEnv()

	cl, err := getRedis()
	if err != nil {
		log.Printf("error loading redis: %s", err)
	}
	redisClient = cl
}

func getRedis() (*redis.Client, error) {
	cl := redis.NewClient(&redis.Options{
		Addr: os.Getenv("REDIS_ADDR"),
		DialTimeout: 1 * time.Second,
	})

	_, err := cl.Ping().Result()
	return cl, err
}

func SetKey(key, value string) error {
	if redisClient == nil {
		return errors.New("redis is not initializaed")
	}
	return redisClient.Set(key, value, 0).Err()
}

func GetKey(key string) (string, error) {
	if redisClient == nil {
		return "", errors.New("redis is not initializaed")
	}

	val, err := redisClient.Get(key).Result()
	if err == redis.Nil {
		return "", nil
	}
	return val, err
}
