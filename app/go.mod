module bitbucket.org/clearlinkit/docker-training

go 1.13

require (
	github.com/gin-gonic/gin v1.5.0
	github.com/go-redis/redis v6.15.7+incompatible
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.9.0 // indirect
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.6.2
)
