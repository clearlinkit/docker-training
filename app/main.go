package main

import (
	"bitbucket.org/clearlinkit/docker-training/cmd"
)

func main() {
	cmd.Execute()
}