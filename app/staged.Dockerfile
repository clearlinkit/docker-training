FROM public.ecr.aws/cldevops/go:1.22 as environment

WORKDIR /app

# same as yum / apt
RUN apk add --no-cache git ca-certificates

ENV GOFLAGS="-mod=vendor"
ENV GOPROXY="https://proxy.golang.org"
ENV GOPRIVATE="bitbucket.org/clearlink*"
ENV PORT="8080"
ENTRYPOINT ["./entrypoint.sh"]

FROM environment AS builder

COPY go.mod go.sum ./
RUN go mod download
COPY . /app
RUN go mod vendor

FROM builder AS built
RUN go build -o training

CMD ["./training", "hello"]
#CMD ./training hello
