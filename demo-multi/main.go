package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
	"time"
)

func main() {
	client := http.Client{
		Transport: &http.Transport{
			DialContext: (&net.Dialer{Timeout: 100 * time.Millisecond}).DialContext,
		},
	}

	nodeId := os.Getenv("NODE_ID")

	http.HandleFunc("/proxy/", func(w http.ResponseWriter, req *http.Request) {
		logRequest(req)
		parts := strings.Split(strings.Trim(req.URL.Path, "/"), "/")
		if len(parts) != 2 {
			mustRespond(w, 400, "invalid path " + req.URL.Path)
			return
		}

		otherNode := parts[1]
		url := fmt.Sprintf("http://%s:8080/ping", otherNode)
		log.Printf("making request to %s", url)
		res, err := client.Get(url)
		if err != nil {
			mustRespond(w, 500, fmt.Sprintf("error talking to %s: %s", otherNode, err.Error()))
			return
		}
		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			mustRespond(w, 500, err.Error())
			return
		}

		w.WriteHeader(res.StatusCode)
		w.Write([]byte(fmt.Sprintf("request from node %s to node %s: %s", nodeId, otherNode, string(body))))
	})

	http.HandleFunc("/ping", func(w http.ResponseWriter, req *http.Request) {
		logRequest(req)
		mustRespond(w, 200, fmt.Sprintf("Hi! I'm node %s", nodeId))
	})

	println("listening on port 8080")
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		panic(err)
	}
}

func logRequest(req *http.Request) {
	log.Printf("[%s] %s %s\n", time.Now().Format(time.RFC3339), req.Method, req.URL.Path)
}

func mustRespond(w http.ResponseWriter, statusCode int, response string) {
	w.Header().Set("content-type", "text/plain")
	w.WriteHeader(statusCode)
	_, err := fmt.Fprintf(w, response)
	if err != nil {
		panic(err)
	}
}
