from django.apps import AppConfig


class DatadictionaryConfig(AppConfig):
    name = 'datadictionary'
