from django.urls import path
from datadictionary import views

urlpatterns = [
    path("", views.home, name="home"),
    path("<int:table_name>/", views.datadictionary_detail, name="datadictionary_detail"),
]
