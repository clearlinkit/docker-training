from django.db import models

class Datawarehouse(models.Model):
    table_name = models.CharField(max_length=100)
    description = models.TextField()
    schema = models.CharField(max_length=20)