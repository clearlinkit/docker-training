from django.shortcuts import render
from datadictionary.models import Datawarehouse


def home(request):
    print(request)
    return render(request, 'home.html')

def datadictionary_index(request):
    datadictionary = Datawarehouse.objects.all()
    context = {
        'datadictionary': datadictionary
    }
    return render(request, 'datadictionary_index.html', context)


def datadictionary_detail(request, table_name):
    datadictionary = Datawarehouse.objects.get(pk=table_name)
    context = {
        'datadictionary': datadictionary
    }
    return render(request, 'datadictionary_detail.html', context)